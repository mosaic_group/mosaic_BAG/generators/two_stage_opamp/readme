# _README_



## Getting started (Opensource DB Flow Variant)

Setup your workspace following the instructions of the [README](https://gitlab.com/mosaic_group/mosaic_BAG/opensource_db_template/-/blob/v1.2_RC/README.md?ref_type=heads).

TODO: add reference to documentation for the gen tool.

## Install generator packages

Install the generator packages for the 2-stage-opamp library using the the following command:
```
cd opensource_db_template
gen add --all-of mosaic_opamp
```

## Trigger flow steps

The common generator (`gen`) tool allows to trigger flow steps for any generator package.
Similar to, i.e., `git`, the `gen` tool provides subcommands for every flow step, such as
- `avail`: see which generator packages are available online
- `add`: install online generator packages to your BAG2 workspace
- `make`: run layout/schematic generation, and LVS
- `lvs`: trigger LVS
- `drc`: trigger DRC
- `new`: create a new generator package based on an existing reference generator package

### Getting help / hints

The `gen` tool has built-in help.

To get an overview over the available subcommands, call:
```
gen --help
```

To get an overview over a specific subcommand and its options, call:
```
gen <subcommand> --help

# for example:
gen add --help
gen make --help
```

### Layout/Schematic generation

Generate the layout and (concrete) schematic:
```
gen make inverter2_gen   # treat inverter2 as if it was a top-cell
```

There is not need to make the leaf-cells before the actual top-cell,
it's only useful to test generators in isolation.

Otherwise, directly gen make is invoked on the top-level cell:
```
gen make opamp_two_stage_fullchip_gen   # TODO: not yet working, see project status
```

But for now, for example mux_2to1_gen would use tgate_gen and inverter2_gen,
so directly after `gen add`, we could call:
```
gen make mux_2to1_gen   # no need to make tgate_gen/inverter2_gen before running this command
```

### LVS / DRC checking

`gen make` will automatically run a LVS check after layout/schematic generation has finished.
To trigger LVS explicitly (again), run:
```
gen lvs inverter2_gen
```

To trigger DRC, run:
```
gen drc inverter2_gen
```

### Simulation (TBD)

The testbench packages must be installed:
```
gen add --all-of mosaic_tb
```

The generator `mosaic/amp_cs_gen` provides an example:
```
gen add mosaic/amp_cs_gen
```

To list the available measurements:
```
gen sim amp_cs_gen
```

To run the measurement `ac_tran`:
```
gen sim -m ac_tran amp_cs_gen
```

## Project status

Currently, not all generators are fully working yet with the opensource-db flow variant, but those are:
- Transistor level
  - tgate_gen
  - inverter2_gen
  - nmos_switch_gen
  - two_stage_opamp_nmos_gen
  - cmfb_gen
  - pseudo_resistor_gen
- First hierarchy level
  - mux_2to1_gen
  - dmux_2to1_gen
  - pseudo_resistor_ctrl_gen
- Capacitor related
  - cap_mom_gen
  - cap_mom_array_gen
- ("real") Resistor related:
  - resistor_diff_gen
  - resistor_array_gen

TODO:
- half_vdd_gen
- buffer_gen
- inverter_en_gen
- opamp_two_stage_top_gen
- opamp_two_stage_fullchip_gen
